package com.zenoyuki.notrrest.repository;

import java.util.List;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.zenoyuki.notrrest.entity.Note;

@Repository
public interface NoteRepository extends CrudRepository<Note, Long> {
	@Query("SELECT n FROM Note n ORDER BY n.date DESC")
	public List<Note> findInPages(Pageable page);
	
	@Query("SELECT n FROM Note n WHERE n.date >= :from AND n.date <= :to ORDER BY n.date DESC")
	public List<Note> findBetweenDates(@Param("from") long from, @Param("to") long to, Pageable page);
	
	@Query("SELECT n FROM Note n WHERE n.note LIKE %:keyword%")
	public List<Note> findKeyword(@Param("keyword") String keyword, Pageable page);
	
	@Query("SELECT MIN(n) FROM Note n")
	public Note findNoteMinDate();
	
	@Query("SELECT MAX(n) FROM Note n")
	public Note findNoteMaxDate();
}
