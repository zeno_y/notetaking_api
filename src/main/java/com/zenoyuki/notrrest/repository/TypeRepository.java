package com.zenoyuki.notrrest.repository;

import org.springframework.data.repository.CrudRepository;

import com.zenoyuki.notrrest.entity.Type;

public interface TypeRepository extends CrudRepository<Type, Long> {

}
