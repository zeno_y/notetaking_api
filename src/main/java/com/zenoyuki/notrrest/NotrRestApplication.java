package com.zenoyuki.notrrest;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

import com.zenoyuki.notrrest.security.AuthenticationSuccessHandler;

@ComponentScan(basePackages = "com.zenoyuki")
@SpringBootApplication
public class NotrRestApplication extends SpringBootServletInitializer {
	
	@Bean
	public AuthenticationSuccessHandler successHandler() {
		return new AuthenticationSuccessHandler();
	}
	
	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(NotrRestApplication.class);
	}
	
	@Override
	public void onStartup(ServletContext servletContext) throws ServletException {
		super.onStartup(servletContext);
		servletContext.addListener(new NotrSessionListener());
	}

	public static void main(String[] args) {
		SpringApplication.run(NotrRestApplication.class, args);
	}
}
