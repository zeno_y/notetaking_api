package com.zenoyuki.notrrest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "notes_table")
public class Note {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "_id")
	private long id;
	
	@Column(name = "date")
	private long date;
	
	@Column(name = "note")
	private String note;
	
//	@ManyToOne
//	@JoinColumn(name = "type_id")
//	private Type type;
	
	@Column(name = "type_id")
	private long typeId;
	
	public Note() {}
	
	public Note(long date, String note, long typeId) {
		setDate(date);
		setNote(note);
		setTypeId(typeId);
	}
	
//	public Note(long id, long date, String note, Type type) {
//		setId(id);
//		setDate(date);
//		setNote(note);
//		setType(type);
//	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

//	public Type getType() {
//		return type;
//	}
//
//	public void setType(Type type) {
//		this.type = type;
//	}

	public long getTypeId() {
		return typeId;
	}

	public void setTypeId(long typeId) {
		this.typeId = typeId;
	}
}
