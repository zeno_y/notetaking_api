package com.zenoyuki.notrrest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "types_table", uniqueConstraints = @UniqueConstraint(columnNames = {"type"}))
public class Type {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "_id")
	private long id;
	
	@Column(name = "type")
	private String type;

//	@JsonIgnore
//	@OneToMany(mappedBy = "type")
//	private List<Note> notes;
	
	public Type() {}
	
	public Type(long id, String type/*, List<Note> notes*/) {
		setId(id);
		setType(type);
//		setNotes(notes);
	}

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

//	public List<Note> getNotes() {
//		return notes;
//	}
//
//	public void setNotes(List<Note> notes) {
//		this.notes = notes;
//	}
//	
//	public void add(Note note) {
//		if(notes == null) notes = new ArrayList<>();
//		notes.add(note);
//		note.setType(Type.this);
//	}
}
