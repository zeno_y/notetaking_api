package com.zenoyuki.notrrest.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationFailureHandler;

@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {
	
	@Autowired
	private AuthenticationEntryPoint entryPoint;
	
	@Autowired
	private AuthenticationSuccessHandler successHandler;
	
	private SimpleUrlAuthenticationFailureHandler failureHandler = 
			new SimpleUrlAuthenticationFailureHandler();
	
	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
		//Encrypted using Bcrypt
		// See aaplication.properties for plain-text
		auth.inMemoryAuthentication().withUser(REDACTED)
			.password(encoder().encode(REDACTED)).roles("USER");
	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.csrf().disable().exceptionHandling().authenticationEntryPoint(entryPoint)
		.and().authorizeRequests()
			.antMatchers("/api/**").authenticated()
		.and().formLogin().successHandler(successHandler).failureHandler(failureHandler)
		.and().logout();
		
//		http.csrf().disable().authorizeRequests().anyRequest()
//			.authenticated().and().httpBasic().authenticationEntryPoint(entryPoint);
	}
	
	@Bean
	public PasswordEncoder encoder() {
		return new BCryptPasswordEncoder();
	}
}
