package com.zenoyuki.notrrest.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.security.core.Authentication;
import org.springframework.security.web.authentication.SimpleUrlAuthenticationSuccessHandler;
import org.springframework.security.web.savedrequest.HttpSessionRequestCache;
import org.springframework.security.web.savedrequest.RequestCache;
import org.springframework.security.web.savedrequest.SavedRequest;
import org.springframework.util.StringUtils;

public class AuthenticationSuccessHandler extends SimpleUrlAuthenticationSuccessHandler {
	
	private RequestCache rCache = new HttpSessionRequestCache();
	
	@Override
	public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response,
			Authentication authentication) throws IOException, ServletException {
		SavedRequest sRequest = rCache.getRequest(request, response);
		
		if(sRequest == null) {
			clearAuthenticationAttributes(request);
			return;
		}
		
		String targetUrlParam = getTargetUrlParameter();
		boolean condition = targetUrlParam != null && StringUtils.hasText(request.getParameter(targetUrlParam));
		
		if(isAlwaysUseDefaultTargetUrl() || condition) {
			rCache.removeRequest(request, response);
			clearAuthenticationAttributes(request);
			return;
		}
		
		clearAuthenticationAttributes(request);
	}
	
	public void setRequestCache(RequestCache rCache) {
		this.rCache = rCache;
	}
}
