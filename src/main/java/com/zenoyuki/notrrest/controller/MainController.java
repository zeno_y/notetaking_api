package com.zenoyuki.notrrest.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.zenoyuki.notrrest.entity.Note;
import com.zenoyuki.notrrest.entity.Type;
import com.zenoyuki.notrrest.repository.NoteRepository;
import com.zenoyuki.notrrest.repository.TypeRepository;

@RequestMapping("/api")
@RestController
public class MainController {
	private static final long ERROR = -1L;
	
	@Autowired
	private NoteRepository noteRepo;
	
	@Autowired
	private TypeRepository typeRepo;
	
	@PostMapping("/add-note")
	public Note addNote(@RequestBody Note note) {
		long date = System.currentTimeMillis();
		note.setDate(date);
		noteRepo.save(note);
		
		return note;
	}
	
	@GetMapping("/all-notes")
	public Iterable<Note> getAllNotes() {
		return noteRepo.findAll();
	}
	
	@GetMapping("/notes-in-pages")
	public List<Note> getNotesInPages(
			@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size) {
		Pageable pageObj = PageRequest.of(page, size);
		return noteRepo.findInPages(pageObj);
	}
	
	@GetMapping("/notes-between-dates")
	public List<Note> getNotesBetweenDates(
			@RequestParam(value = "from") long from,
			@RequestParam(value = "to") long to,
			@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size) {
		Pageable pageObj = PageRequest.of(page, size);
		return noteRepo.findBetweenDates(from, to, pageObj);
	}
	
	@GetMapping("/notes-with-keyword")
	public List<Note> getNotesWithKeywords(
			@RequestParam(value = "keyword") String keyword,
			@RequestParam(value = "page") int page,
			@RequestParam(value = "size") int size) {
		Pageable pageObj = PageRequest.of(page, size);
		return noteRepo.findKeyword(keyword, pageObj);
	}
	
	@GetMapping("/min-date")
	public long getMinDate() {
		final Note minNote = noteRepo.findNoteMinDate();
		return minNote != null ? minNote.getDate() : ERROR;
	}
	
	@GetMapping("/max-date")
	public long getMaxDate() {
		final Note maxNote = noteRepo.findNoteMaxDate();
		return maxNote != null ? maxNote.getDate() : ERROR;
	}
}
