package com.zenoyuki.notrrest.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class FeedbackController {
	@RequestMapping("/")
	public @ResponseBody String getFeedback() {
		return "Notr: ready";
	}
}
